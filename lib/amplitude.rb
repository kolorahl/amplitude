require 'faraday'

module Amplitude
  require_relative 'amplitude/errors'

  autoload :Config, "#{__dir__}/amplitude/config.rb"
  autoload :Event, "#{__dir__}/amplitude/event.rb"
  autoload :Formatter, "#{__dir__}/amplitude/formatter.rb"
  autoload :HashSlicer, "#{__dir__}/amplitude/hash_slicer.rb"

  module Formatters
    autoload :Hash, "#{__dir__}/amplitude/formatters/hash.rb"
    autoload :Time, "#{__dir__}/amplitude/formatters/time.rb"
  end

  module Jobs
    autoload :Default, "#{__dir__}/amplitude/jobs/default.rb"
  end

  API_URI = 'https://api.amplitude.com/httpapi'

  class << self
    def config
      Amplitude::Config.instance
    end

    def configure
      yield config
    end

    def api_key(options = {})
      options.fetch(:api_key, Amplitude.config.api_key)
    end

    def create_event(data)
      data.is_a?(Event) ? data : Event.new(data)
    end

    def track(data, options = {})
      raise APIKeyError unless (key = api_key(options))
      Faraday.post(API_URI, api_key: key, event: create_event(data).to_json)
    end

    def track!(data, options = {})
      response = track(data, options)
      raise APIError, response unless response.success?
      response
    end

    def queue(data, options = {})
      raise AsyncError unless async_enabled?
      Amplitude::Jobs::Default.perform_later(create_event(data).to_json, options)
    end

    def async_enabled?
      defined?(ActiveJob)
    end
  end
end
