require 'active_job'

class Amplitude::Jobs::Default < ActiveJob::Base
  queue_as Amplitude.config.job_queue

  def perform(data, options)
    Amplitude.track!(deserialize(data), options)
  end

  def deserialize(data)
    return data if data.is_a?(Amplitude::Event) || data.is_a?(Hash)
    MultiJson.load(data, symbolize_keys: true)
  end
end
