module Amplitude::Formatters
  class Hash
    class << self
      def call(obj)
        obj&.to_h
      end
    end
  end
end
