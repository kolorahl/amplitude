module Amplitude::Formatters
  class Time
    class << self
      def call(time)
        return time if time.nil?
        guess = time.to_i
        ::Time.at(guess) > ::Time.now ? guess : convert_to_ms(time)
      end

      def convert_to_ms(time)
        (time.to_f * 1_000).to_i
      end
    end
  end
end
