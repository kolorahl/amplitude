require 'multi_json'

class Amplitude::Event
  def initialize(data)
    @data = Amplitude::Formatter.call(data)
  end

  def as_json
    @data.dup
  end

  def to_json
    MultiJson.dump(@data)
  end

  def method_missing(method, *args)
    return @data[method] if @data.key?(method)
    super
  end

  def respond_to_missing?(method, include_all = false)
    return true if @data.key?(method)
    super
  end
end
