class Amplitude::Formatter
  class << self
    def call(data)
      data = sanitize_data(data)
      data = format_data(data)
      add_insert_id(data)
    end

    def sanitize_data(data)
      Amplitude::HashSlicer.call(data, Amplitude.config.whitelist)
    end

    def format_data(data)
      format_props(format_time(data))
    end

    def format_time(data)
      format_if_present(data, :time, time_formatter)
    end

    def format_props(data)
      data = format_if_present(
        data,
        :event_properties,
        event_prop_formatter
      )
      format_if_present(
        data,
        :user_properties,
        user_prop_formatter
      )
    end

    def format_if_present(data, key, formatter)
      return data unless data[key]
      data[key] = formatter.call(data[key])
      data
    end

    def add_insert_id(data)
      return data if data[:insert_id] || Amplitude.config.iid_generator.nil?
      data[:insert_id] = Amplitude.config.iid_generator.call()
      data
    end

    protected

    def time_formatter
      Amplitude.config.time_formatter
    end

    def event_prop_formatter
      Amplitude.config.event_prop_formatter
    end

    def user_prop_formatter
      Amplitude.config.user_prop_formatter
    end
  end
end
