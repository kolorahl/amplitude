class Amplitude::HashSlicer
  class << self
    def call(obj, *keys)
      return {} if keys.length == 0

      keys = keys[0] if keys[0].is_a?(Array)
      hash = obj.to_h

      if hash.respond_to?(:slice)
        slice_hash(hash, keys)
      else
        map_hash(hash, keys)
      end
    end

    private

    def slice_hash(hash, keys)
      hash.slice(*keys)
    end

    def map_hash(hash, keys)
      pick = hash.keys & keys
      pick.map { |key| [key, hash[key]] }.to_h
    end
  end
end
