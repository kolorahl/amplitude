require 'securerandom'
require 'singleton'

class Amplitude::Config
  include Singleton

  attr_accessor :api_key, :secret_key, :env_var, :job_queue, :whitelist,
  :time_formatter, :event_prop_formatter, :user_prop_formatter, :iid_generator

  def initialize
    reset
  end

  def reset
    self.class.defaults.each { |k, v| send("#{k}=", v) }
  end

  def api_key
    @api_key ||= ENV[env_var]
  end

  class << self
    def defaults
      {
        api_key: nil,
        secret_key: nil,
        env_var: 'AMPLITUDE_API_KEY',
        job_queue: :default,
        whitelist: %i(
          user_id device_id event_type time groups app_version ip platform
          os_name os_version device_brand device_manufacturer device_model
          carrier country region city dma language price quantity revenue
          productId revenueType location_lat location_lng idfa idfv adid
          android_id event_properties user_properties insert_id session_id
          event_id
        ),
        time_formatter: Amplitude::Formatters::Time,
        event_prop_formatter: Amplitude::Formatters::Hash,
        user_prop_formatter: Amplitude::Formatters::Hash,
        iid_generator: ->() { SecureRandom.uuid }
      }
    end
  end
end
