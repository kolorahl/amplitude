module Amplitude
  class Error < StandardError
  end

  class APIKeyError < Error
    def initialize
      super('API key is missing or invalid')
    end
  end

  class AsyncError < Error
    def initialize
      super('Asynchronous operations not permitted')
    end
  end

  class APIError < Error
    attr_accessor :url, :code, :body

    def initialize(response)
      super('The HTTP API returned an error')
      self.url = response.request.path.to_s
      self.code = response.status
      self.body = response.body
    end
  end
end
