RSpec.describe Amplitude do
  subject { described_class }

  let(:api_key) { 'test' }
  let(:data) do
    {
      event_type: 'test',
      user_id: 1234,
      time: Time.now
    }
  end

  describe '.config' do
    subject { described_class.config }

    it 'returns an instance of Amplitude::Config' do
      expect(subject).to be_a(Amplitude::Config)
    end
  end

  describe '.configure' do
    it 'yields an instance of Amplitude::Config' do
      expect(described_class).to receive(:config).and_call_original
      subject.configure do |config|
        expect(config).to be_a(Amplitude::Config)
      end
    end
  end

  describe '.api_key' do
    subject { described_class.api_key(options) }

    let(:options) { {} }
    let(:config_api_key) { nil }
    let(:env_api_key) { nil }

    shared_examples 'api key fetching' do
      before do
        Amplitude.config.api_key = config_api_key
        ENV[Amplitude.config.env_var] = env_api_key
      end

      it 'fetchs the correct API key' do
        expect(subject).to eq(api_key)
      end
    end

    context 'when an API key is provided' do
      context 'by the options hash' do
        let(:options) { { api_key: api_key } }

        include_examples 'api key fetching'
      end

      context 'by manual configuration' do
        let(:config_api_key) { api_key }

        include_examples 'api key fetching'
      end

      context 'by the environment' do
        let(:env_api_key) { api_key }

        include_examples 'api key fetching'
      end

      context 'when all key configurations are used' do
        let(:options) { { api_key: api_key } }
        let(:config_api_key) { 'config' }
        let(:env_api_key) { 'env' }

        include_examples 'api key fetching'
      end

      context 'when config and env configurations are used' do
        let(:config_api_key) { api_key }
        let(:env_api_key) { 'env' }

        include_examples 'api key fetching'
      end

      context 'when options and env configurations are used' do
        let(:options) { { api_key: api_key } }
        let(:env_api_key) { 'env' }

        include_examples 'api key fetching'
      end

      context 'when options and config configurations are used' do
        let(:options) { { api_key: api_key } }
        let(:config_api_key) { 'config' }

        include_examples 'api key fetching'
      end
    end
  end

  describe '.create_event' do
    subject { described_class.create_event(arg) }
    let(:event) { Amplitude::Event.new(data) }

    context 'when given an Event object' do
      let!(:arg) { event }

      it 'returns it immediately' do
        expect(Amplitude::Event).to_not receive(:new)
        expect(subject).to eq(event)
      end
    end

    context 'when given a hash' do
      let!(:arg) { data }

      it 'creates a new Event object' do
        expect(Amplitude::Event).to receive(:new).with(data).and_return(event)
        expect(subject).to eq(event)
      end
    end
  end

  describe '.track' do
    subject { described_class.track(data) }

    context 'without an API key set' do
      before { Amplitude.config.api_key = nil }

      it 'raises an error' do
        expect { subject }.to raise_error(Amplitude::APIKeyError)
      end
    end

    context 'with an API key set' do
      before { Amplitude.config.api_key = api_key }

      it 'posts the event to Amplitude' do
        expect(Faraday).to receive(:post).with(Amplitude::API_URI, hash_including(api_key: api_key)).and_return(true)
        expect(subject).to be_truthy
      end
    end
  end

  describe '.track!' do
    subject { described_class.track!(data) }

    before do
      Amplitude.config.api_key = api_key
      expect(Faraday).to receive(:post).with(Amplitude::API_URI, hash_including(api_key: api_key, event: anything)).and_return(response)
    end

    context 'on a successful response' do
      let(:response) do
        Hashie::Mash.new(status: 200, success?: true, body: 'yay')
      end

      it 'calls .track without raising an error' do
        expect { subject }.to_not raise_error
      end

      it 'returns a response object' do
        expect(subject.status).to eq(200)
        expect(subject.body).to eq('yay')
      end
    end

    context 'on an unsuccessful response' do
      let(:response) do
        Hashie::Mash.new(status: 400, success?: false, body: 'oh no', request: { path: '/' })
      end

      it 'calls .track and raises an error' do
        expect { subject }.to raise_error(Amplitude::APIError)
      end
    end
  end

  describe '.queue' do
    subject { described_class.queue(data) }

    context 'when ActiveJob is not defined' do
      before do
        allow(described_class).to receive(:async_enabled?).and_return(false)
      end

      it 'raises an error' do
        expect { subject }.to raise_error(Amplitude::AsyncError)
      end
    end

    context 'when ActiveJob is defined' do
      let(:event) { Amplitude::Event.new(data) }

      before do
        allow(Amplitude).to receive(:create_event).with(data).and_return(event)
      end

      it 'ensures :insert_id is present before enqueuing the job' do
        expect(Amplitude::Jobs::Default).to receive(:perform_later).with(event.to_json, {}).and_return(true)
        expect(subject).to be_truthy
      end
    end
  end
end
