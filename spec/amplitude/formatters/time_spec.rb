RSpec.describe Amplitude::Formatters::Time do
  let(:now) { Time.now }

  describe '.call' do
    subject { described_class.call(time) }

    context 'given seconds as an integer' do
      let(:time) { now.to_i }

      it 'converts to milliseconds' do
        expect(described_class).to receive(:convert_to_ms).with(time).and_return(true)
        expect(subject).to be_truthy
      end
    end

    context 'given milliseconds as an integer' do
      let(:time) { now.to_f * 1_000 }

      it 'performs no conversion' do
        expect(described_class).to_not receive(:convert_to_ms)
        expect(subject).to eq(time.to_i)
      end
    end

    context 'given a Time object' do
      let(:time) { now }

      it 'converts to milliseconds' do
        expect(described_class).to receive(:convert_to_ms).with(time).and_return(true)
        expect(subject).to be_truthy
      end
    end

    context 'given nil' do
      let(:time) { nil }
      
      it 'return nil' do
        expect(described_class).to_not receive(:convert_to_ms)
        expect(subject).to be_nil
      end
    end
  end

  describe '.convert_to_ms' do
    subject { described_class.convert_to_ms(time) }

    shared_examples 'time conversion' do
      it 'multiples the time by 1,000 and returns an integer' do
        expect(subject).to eq((time.to_f * 1_000).to_i)
      end
    end

    context 'given an Integer' do
      let(:time) { 1234 }

      include_examples 'time conversion'
    end

    context 'given a Float' do
      let(:time) { 1234.56789 }

      include_examples 'time conversion'
    end

    context 'given a Time object' do
      let(:time) { now }

      include_examples 'time conversion'
    end
  end
end
