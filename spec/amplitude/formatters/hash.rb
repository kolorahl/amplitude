RSpec.describe Amplitude::Formatters::Hash do
  describe '.call' do
    subject { described_class.call(obj) }

    context 'given nil' do
      let(:obj) { nil }

      it 'returns nil' do
        expect(obj).to_not receive(:to_h)
        expect(subject).to be_nil
      end
    end

    context 'given an object responding to #to_h' do
      let(:result) { { success: true } }
      let(:obj) { double('object') }

      it 'calls to_h and returns the result' do
        expect(obj).to receive(:to_h).and_return(result)
        expect(subject).to eq(result)
      end
    end

    context 'given an object not responding to #to_h' do
      let(:obj) { double('object') }

      it 'raises an error' do
        expect { subject }.to raise_error(NoMethodError)
      end
    end
  end
end
