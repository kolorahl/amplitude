RSpec.describe Amplitude::Event do
  subject { described_class.new(data) }

  let(:user_id) { 1234 }
  let(:event_type) { 'test' }
  let(:event_properties) { { any: 'thing' } }
  let(:now) { Time.now }

  let(:data) do
    { user_id: user_id, event_type: event_type, event_properties: event_properties, time: now }
  end

  before do
    Amplitude.config.iid_generator = nil
  end

  describe '#initialize' do
    it 'formats the data' do
      expect(Amplitude::Formatter).to receive(:call).with(data).and_return(data)
      subject
    end
  end

  describe '#as_json' do
    let(:formatted_data) { data.merge(time: now.to_i) }

    before do
      allow(Amplitude::Formatter).to receive(:call).with(data).and_return(formatted_data)
    end

    it 'returns the formatted data' do
      expect(subject.as_json).to eq(formatted_data)
    end

    it 'returns a dup' do
      expect(formatted_data).to receive(:dup).twice.and_call_original
      copy = subject.as_json
      copy[:user_id] *= 2
      expect(formatted_data[:user_id]).to eq(user_id)
      expect(subject.as_json).to eq(formatted_data)
    end
  end

  describe '#to_json' do
    it 'creates an approriate JSON string of the event data' do
      json = subject.to_json
      expect(json).to be_a(String)
      expect(MultiJson.load(json, symbolize_keys: true)).to eq(data.merge(time: (now.to_f * 1_000).to_i))
    end
  end

  describe 'method delegation' do
    it 'will act like mashie for valid attributes present in the data hash' do
      expect(subject.respond_to?(:user_id)).to be_truthy
      expect(subject.user_id).to eq(user_id)
    end

    it 'will raise an error for attributes not present in the data hash' do
      expect(subject.respond_to?(:invalid)).to be_falsey
      expect { subject.invalid }.to raise_error(NoMethodError)
    end
  end
end
