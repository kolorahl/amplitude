RSpec.describe Amplitude::Jobs::Default do
  let(:instance) { described_class.new }
  let(:data) do
    {
      event_type: 'test',
      user_id: 1234,
      insert_id: 'abc-123',
      time: Time.now
    }
  end
  let(:event) { Amplitude::Event.new(data) }
  let(:dump) { event.to_json }

  describe '#deserialize' do
    context 'when given an event or hash' do
      it 'returns the given object' do
        expect(MultiJson).to_not receive(:load)
        expect(instance.deserialize(data)).to eq(data)
        expect(instance.deserialize(event)).to eq(event)
      end
    end

    context 'when given an JSON dump' do
      it 'returns the deserialized object' do
        expect(MultiJson).to receive(:load).with(dump, symbolize_keys: true).and_call_original
        expect(instance.deserialize(dump)).to eq(event.as_json)
      end
    end
  end

  describe '#perform' do
    subject { instance.perform(dump, {}) }

    before do
      allow(instance).to receive(:deserialize).with(dump).and_return(data)
    end

    it 'calls Amplitude.track!' do
      expect(Amplitude).to receive(:track!).with(data, {}).and_return(true)
      expect(subject).to be_truthy
    end
  end
end
