RSpec.describe Amplitude::HashSlicer do
  let(:hash) do
    {
      one: 'one',
      two: 2,
      three: :drei,
      four: ['quattro']
    }
  end
  let(:expected_hash) do
    { two: 2, three: :drei }
  end
  let(:keys) { [:two, :three] }

  describe '.call' do
    shared_examples 'slicing' do
      context 'with argument list of keys' do
        subject { described_class.call(hash, *keys) }

        before do
          expect(described_class).to receive(:call).with(hash, *keys).and_call_original
        end

        it do
          expect(subject).to eq(expected_hash)
        end
      end

      context 'with array of keys' do
        subject { described_class.call(hash, keys) }

        before do
          expect(described_class).to receive(:call).with(hash, keys).and_call_original
        end

        it do
          expect(subject).to eq(expected_hash)
        end
      end
    end

    context 'with a hash that responds to :slice' do
      before do
        unless hash.respond_to?(:slice)
          allow(hash).to receive(:respond_to?).with(:slice, any_args).and_return(true)
          expect(hash).to receive(:slice).with(*keys).and_return(expected_hash)
        end
      end

      include_examples 'slicing'
    end

    context 'with a hash that does not respond to :slice' do
      before do
        allow(hash).to receive(:respond_to?).with(:slice, any_args).and_return(false)
      end

      include_examples 'slicing'
    end
  end
end
