RSpec.describe Amplitude::Formatter do
  subject { described_class }

  let(:user_id) { 1234 }
  let(:event_type) { 'test' }
  let(:event_properties) { { any: 'thing' } }
  let(:now) { Time.now }

  let(:invalid_data) do
    { nope: user_id, ignore: event_type, invalid_properties: event_properties }
  end
  let(:valid_data) do
    { user_id: user_id, event_type: event_type, event_properties: event_properties, time: now }
  end
  let(:mixed_data) do
    invalid_data.merge(valid_data)
  end

  describe '.call' do
    subject { described_class.call(mixed_data) }

    it 'calls the appropriate functions' do
      expect(described_class).to receive(:sanitize_data).with(mixed_data).and_return(valid_data)
      expect(described_class).to receive(:format_data).with(valid_data).and_return(valid_data)
      expect(subject).to eq(valid_data)
    end
  end

  describe '.sanitize_data' do
    it 'keeps only whitelisted keys' do
      expect(subject.sanitize_data(invalid_data)).to be_empty
      expect(subject.sanitize_data(mixed_data)).to eq(valid_data)
    end
  end

  describe '.format_if_present' do
    subject { described_class.format_if_present(valid_data, key, formatter) }
    let(:formatter) do
      ->(x) { x * 2 }
    end

    context 'when key is present' do
      let(:key) { :user_id }

      it 'calls the formatter and sets the hash value' do
        expect(formatter).to receive(:call).with(user_id).and_call_original
        expect(subject).to eq(valid_data.merge(user_id: user_id * 2))
      end
    end

    context 'when key is not present' do
      let(:key) { :invalid }

      it 'does not call the formatter' do
        expect(formatter).to_not receive(:call)
        expect(subject).to eq(valid_data)
      end
    end
  end

  describe '.format_time' do
    subject { described_class.format_time(time: now) }

    it 'retrieves the configured formatter' do
      expect(Amplitude.config).to receive_message_chain(:time_formatter, :call).and_return(true)
      expect(subject).to be_truthy
    end
  end

  describe '.format_props' do
    subject { described_class.format_props(valid_data) }

    it 'calls the appropriate functions' do
      expect(described_class).to receive(:format_if_present)
      .with(valid_data, :event_properties, Amplitude.config.event_prop_formatter)
      .and_return(valid_data)
      expect(described_class).to receive(:format_if_present)
      .with(valid_data, :user_properties, Amplitude.config.user_prop_formatter)
      .and_return(valid_data)
      subject
    end
  end

  describe '.format_data' do
    subject { described_class.format_data(step_one) }

    let(:step_one) { valid_data }
    let(:step_two) { step_one.merge(time: now.to_i * 1_000) }
    let(:step_three) { step_two.merge(user_properties: { swamp: 'thing' }) }

    it 'calls the appropriate functions' do
      expect(described_class).to receive(:format_time).with(step_one).and_return(step_two)
      expect(described_class).to receive(:format_props).with(step_two).and_return(step_three)
      expect(subject).to eq(step_three)
    end
  end

  describe '.add_insert_id' do
    subject { described_class.add_insert_id(data) }

    let(:data) { valid_data }

    context 'with the default :iid_generator config' do
      it 'generates a random UUID for the :insert_id' do
        expect(SecureRandom).to receive(:uuid).and_call_original
        expect(subject).to include(:insert_id)
      end
    end

    context 'with a nil :iid_generator config' do
      before { Amplitude.config.iid_generator = nil }

      it 'does not add :insert_id' do
        expect(SecureRandom).to_not receive(:uuid)
        expect(subject).to_not include(:insert_id)
      end
    end

    context 'when :insert_id is already present' do
      let(:insert_id) { 'abcd-1234-hex' }
      let(:data) { valid_data.merge(insert_id: insert_id) }

      it 'does not overwrite :insert_id' do
        expect(SecureRandom).to_not receive(:uuid)
        result = subject
        expect(result).to include(:insert_id)
        expect(result[:insert_id]).to eq(insert_id)
      end
    end
  end
end
