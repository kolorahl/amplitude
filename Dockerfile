FROM ruby:2.5-alpine3.7

RUN apk --no-cache add build-base

RUN mkdir -p /root/amplitude
WORKDIR /root/amplitude

COPY .rspec .
COPY amplitude-rb.gemspec .
COPY Gemfile .
COPY Gemfile.lock .

RUN bundle install

COPY lib lib
COPY spec spec

CMD ["sh"]
