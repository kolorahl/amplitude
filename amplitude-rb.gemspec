Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'amplitude-rb'
  s.version     = '0.2.1'

  s.summary     = 'Communicates with Amplitude HTTP API'
  s.description = 'Provides a simple Ruby library to communicate with the Amplitude HTTP API.'

  s.authors     = ['Tyler Eon']
  s.email       = 'kolorahl@gmail.com'
  s.homepage    = 'https://gitlab.com/kolorahl/amplitude'
  s.license     = 'GPL-3.0'

  s.files       = Dir['LICENSE', 'README.md', 'Gemfile', 'Gemfile.lock', 'lib/**/*.rb']

  s.add_dependency 'faraday', '~> 0.14.0'
  s.add_dependency 'multi_json', '~> 1.13'
end
